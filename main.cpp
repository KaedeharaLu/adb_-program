#include<iostream>
#include<windows.h>
#include<fstream>
#include <stdio.h>
#include <string.h>

using namespace std;

char option;
string path,preCmd,fullCmd,deviceList[25],deviceName[25],deviceTag[25],tagIp[105]={},tags[105]={};
int tagNum=0,deviceNum=0,option1Num=0;

string getCmdResult(const string &strCmd)  
{
    char buf[10240] = {0};
    FILE *pf = NULL;
 
    if( (pf = popen(strCmd.c_str(), "r")) == NULL )
    {
        return "";
    }
 
    string strResult;
    while(fgets(buf, sizeof buf, pf))
    {
        strResult += buf;
    }
 
    pclose(pf);
 
    unsigned int iSize =  strResult.size();
    if(iSize > 0 && strResult[iSize - 1] == '\n')  // linux
    {
        strResult = strResult.substr(0, iSize - 1);
    }
 
    return strResult;
}

void setTags(){
    //比对并赋予标签
    for(int i=1;i<=deviceNum;i++){
        for(int k=1;k<=tagNum;k++){
            if(deviceList[i]==tagIp[k]){
                deviceTag[i]=tags[k];
                break;
            }else{
                deviceTag[i]=="Unkown";
            }
        }
    }
    return;
}

void saveTags(){
    fstream tagOut;
    remove("tags.json");
    tagOut.open("tags.json",ios::out);
    tagOut<<tagNum<<endl;
    for(int i=1;i<=tagNum;i++){
        tagOut<<tagIp[i]<<"\t"<<tags[i]<<endl;
    }
    return;
}

void start(){//开始运行
    system("title ADB-Helper by KaedeharaLu");
    system("color 07");
    system("cls");
    cout<<"欢迎使用由KaedeharaLu开发的ADB调试辅助工具箱"<<endl;
    cout<<"注:下载的源代码包中的两个json文件为作者的测试文件,请先删除再使用!"<<endl;
    cout<<"按下任意键开始启动!"<<endl;
    system("pause");
    cout<<"----------------------------------------------------------------------"<<endl;
    cout<<"准备开启adb服务"<<endl;
    system("adb start-server");
    cout<<"完成!"<<endl;
    cout<<"----------------------------------------------------------------------"<<endl;

    //检验已连接设备
    cout<<"检验已连接的设备······"<<endl;
    fullCmd="adb devices";
    string result=getCmdResult(fullCmd);
    char res;
    int ippd=0,namepd=0;//ippd:判断是否查找ip    namepd:用来读取设备名称
    deviceNum=0;
    for(int i=0;i<=result.length();i++){
        res=result[i];
        int ascii=res;
        if((ascii>=48&&ascii<=57)||ascii==46||ascii==58||namepd==1){//48~57:0~9  46is.   58is:  65~90:A~Z   97~122:a~z
            if(ippd==0){
                deviceNum++;
                deviceList[deviceNum]="";
                deviceName[deviceNum]="";
                ippd=1;
            }
            if((ascii>=48&&ascii<=57)||ascii==46||ascii==58) deviceList[deviceNum]+=res;
            if(namepd==1&&result[i+1]==9){
                namepd=0;
            }
            if(namepd==1&&ascii!=9&&ascii!=10){
                deviceName[deviceNum]+=res;
            }
            if(result[i+1]==9&&namepd!=1){
                namepd=1;
            }
        }else{
            ippd=0;
        }
    }
    if(deviceNum==0){
        cout<<"当前无设备连接"<<endl;
    }

    //读取tags
    fstream tagIn;
    tagIn.open("tags.json",ios::in);
    tagIn>>tagNum;
    for(int i=1;i<=tagNum;i++){
        tagIn>>tagIp[i]>>tags[i];
    }
    tagIn.close();
    setTags();//设置tags
    cout<<"----------------------------------------------------------------------"<<endl;
    cout<<"共读取到"<<tagNum<<"个标签,如下:"<<endl;
    for(int i=1;i<=tagNum;i++){
        cout<<i<<":"<<tagIp[i]<<"\t"<<tags[i]<<endl;
    }

    cout<<"----------------------------------------------------------------------"<<endl;
    cout<<"当前共"<<deviceNum<<"个设备连接,信息如下:"<<endl;
    for(int i=1;i<=deviceNum;i++){
        cout<<i<<".设备名称:"<<deviceName[i]<<"\tip:"<<deviceList[i]<<"\t标签:"<<deviceTag[i]<<endl;
    }
    cout<<"----------------------------------------------------------------------"<<endl;
    cout<<"启动完成!按下任意键进入程序!"<<endl;
    system("pause");
    return;
}

void onLoad(string title,int clean){//设置title并清屏
    if(clean==1) system("cls");
    preCmd="title ";
    fullCmd=preCmd+title;
    system(fullCmd.c_str());
    return;
}

void onShow(){//后面回到主页显示
    onLoad("ADB-Helper_by_Kaedehara",1);
    cout<<"0.关闭adb服务并退出"<<endl;
    cout<<"1.连接adb设备"<<"\t\t\t"<<"2.查看已连接的设备"<<endl;
    cout<<"3.安装apk"<<endl;
    cout<<"----------------------------------------------------------------------"<<endl;
    return;
}

void option1_self(){
    onLoad("连接设备-手动",1);
    string adbAdress;
    preCmd="adb connect ";
    cout<<"请输入连接地址(ip和端口一起输,中间用\":\"连接;输入0返回):";
    cin>>adbAdress;
    fullCmd=preCmd+adbAdress;
    // system(fullCmd.c_str());
    if(adbAdress=="0"){
        return;
    }
    cout<<"尝试连接中,请稍后······"<<endl;
    string result=getCmdResult(fullCmd);
    char res[result.length()+5];
    string output[2]={"already","connected"},temp;

    //判断返回结果：已经连接：already……，连接成功：conneed……，连接失败：其他的
    for(int i=0;i<=10;i++){
        for(int j=0;j<=1;j++){//判断前两种结果
            temp="";
            for(int k=0;k<=output[j].length()-1;k++){//将值赋给temp
                temp+=result[i+k];
            }
            if(temp==output[0]){//依次判断字符串“already”和“connected”
                //当前设备已连接
                cout<<"当前设备已连接"<<endl;
                system("pause");
                return;
            }else if(temp==output[1]){
                //未连接且连接成功
                cout<<"连接成功!"<<endl;
                deviceNum++;
                deviceList[deviceNum]=adbAdress;
                deviceName[deviceNum]="Undefined";
                deviceTag[deviceNum]="Unkown";
                tagIp[deviceNum]=adbAdress;
                tags[deviceNum]="Unkown";

                //保存列表
                fstream ipOut;
                remove("deviceList.json");
                ipOut.open("deviceList.json",ios::out);
                ipOut<<deviceNum<<endl;
                for(int i=1;i<=deviceNum;i++){
                    ipOut<<deviceName[i]<<"\t"<<deviceList[i]<<endl;
                }
                ipOut.close();

                //保存标签
                fstream tagOut;
                remove("tags.json");
                tagOut.open("tags.json",ios::out);
                tagOut<<tagNum<<endl;
                for(int k=1;k<=tagNum;k++){
                    tagOut<<tagIp[k]<<"\t"<<tags[k]<<endl;
                }
                tagOut.close();

                system("pause");
                return;
            }
        }

        //不属于前两种情况，即连接失败
        cout<<"连接失败!错误信息如下:"<<endl;
        cout<<result<<endl;
        system("pause");
        return;
    }
}

void option1_auto(){
    onLoad("连接设备-自动",1);
    cout<<"即将自动读取当前目录下\"deviceList.json\"文件"<<endl;
    Sleep(500);
    cout<<"开始读取······"<<endl;

    int numTemp;//读取的设备数
    string ipTemp[25],nameTemp[25];//读取的ip

    //开始读取
    fstream ipGetin;
    ipGetin.open("deviceList.json",ios::in);
    ipGetin>>numTemp;
    for(int i=1;i<=numTemp;i++){
        ipGetin>>nameTemp[i]>>ipTemp[i];
    }
    cout<<"读取完成!"<<endl;
    
    //显示读取到的设备
    cout<<"共读取到"<<numTemp<<"个ip地址,如下:"<<endl;
    for(int i=1;i<=numTemp;i++){
        cout<<i<<"."<<nameTemp[i]<<":"<<ipTemp[i]<<endl;
    }
    cout<<"----------------------------------------------------------------------"<<endl;
    Sleep(500);

    //尝试依次连接设备
    cout<<"开始尝试连接······"<<endl;
    for(int q=1;q<=numTemp;q++){
        fullCmd="adb connect "+ipTemp[q];
        string result=getCmdResult(fullCmd);
        char res[result.length()+5];
        string output[2]={"already","connected"},temp;
        bool skip=false;/*是否跳过显示错误信息:
                            1.识别出connected或already则跳过,显示连接成功
                            2.未识别出，则显示*/

        //判断返回结果：已经连接：already……，连接成功：conneed……，连接失败：其他的
        for(int j=0;j<=1;j++){
            skip=false;
            for(int i=0;i<=10;i++){
                temp="";
                for(int k=0;k<=output[j].length()-1;k++){//将值赋给temp
                    temp+=result[i+k];
                }
                if(temp==output[0]){//依次判断字符串“already”和“connected”
                    //当前设备已连接
                    cout<<"当前设备已连接,ip为:"<<ipTemp[q]<<endl<<endl;
                    int beNoted=0;//判断该已连接的设备是否被记录 0:未记录  1:已经记录
                    for(int q=1;q<=deviceNum;q++){
                        if(ipTemp[i]==deviceList[q]){
                            beNoted=1;
                        }
                    }
                    if(beNoted==0){
                        deviceNum++;
                        deviceList[deviceNum]=ipTemp[q];
                        deviceName[deviceNum]=nameTemp[q];
                    }
                    skip=true;
                    cout<<"----------------------------------------------------------------------"<<endl;
                    goto breakCheck;
                }else if(temp==output[1]){
                    //未连接且连接成功
                    cout<<"连接成功,ip为:"<<ipTemp[q]<<endl<<endl;
                    deviceNum++;
                    deviceList[deviceNum]=ipTemp[q];
                    deviceName[deviceNum]=nameTemp[q];
                    skip=true;
                    cout<<"----------------------------------------------------------------------"<<endl;
                    goto breakCheck;
                }
            }

            if(skip!=true&&j==1){//不属于前两种情况，即连接失败
                cout<<"连接失败!ip为:"<<ipTemp[q]<<",可尝试手动连接获得详细信息"<<endl;
                cout<<"----------------------------------------------------------------------"<<endl;
            }

breakCheck:
            break;
        }
    }

    //比对tagIp
    setTags();

    //保存连接信息
    remove("deviceList.json");
    fstream ipOut;
    ipOut.open("deviceList.json",ios::out);
    ipOut<<deviceNum<<endl;
    for(int i=1;i<=deviceNum;i++){
        ipOut<<deviceList[i]<<endl;
    }

    return;
}

void option1(){
    onLoad("连接adb设备",1);
    if(option1Num==0){//判断是否为第一次进入连接设备
        cout<<"0.返回"<<endl;
        cout<<"1.读取上次连接结果并自动尝试连接"<<endl;
        cout<<"2.手动输入"<<endl;
        cout<<"----------------------------------------------------------------------"<<endl;
        cout<<"请输入操作:";
selectOption1:
        cin>>option;
        if(option=='1'){
            option1_auto();
            system("pause");
            option1Num++;
            return;
        }else if(option=='2'){
            option1_self();
            option1Num++;
            return;
        }else if(option=='0'){
            return;
        }else{
            cout<<"错误!未知操作!请重新输入:"<<endl;
            goto selectOption1;
        }
    }else{
        option1_self();
        return;
    }
    return;
}

void option2_chageTag(){
    onLoad("更改设备标签",0);

    int changeNum;
    string changeTag;
    cout<<"----------------------------------------------------------------------"<<endl;
    cout<<"请输入需要更改的设备的序号(0返回):";
option2_chageTagInputNum:
    cin>>changeNum;
    if(changeNum<0||changeNum>deviceNum){
        cout<<"错误!未知设备!请重新输入:";
        goto option2_chageTagInputNum;
    }
    if(changeNum==0) return;
    cout<<"请输入需要更改的标签(名称中不应出现空格,否则取空格前的部分):";
    cin>>changeTag;

    //更改标签
    deviceTag[changeNum]=changeTag;
    for(int i=1;i<=deviceNum;i++){
        for(int k=1;k<=tagNum;k++){
            if(deviceList[i]==tagIp[k]){
                tags[k]=changeTag;
                goto exitChange;
            }
        }
    }

exitChange:

    //输出保存
    fstream ipOut;
    remove("deviceList.json");
    ipOut.open("deviceList.json",ios::out);
    ipOut<<deviceNum<<endl;
    cout<<"----------------------------------------------------------------------"<<endl;
    cout<<endl<<"当前共"<<deviceNum<<"个设备连接,信息如下:"<<endl;
    for(int i=1;i<=deviceNum;i++){
        cout<<i<<".设备名称:"<<deviceName[i]<<"\tip:"<<deviceList[i]<<"\t标签:"<<deviceTag[i]<<endl;
        ipOut<<deviceName[i]<<"\t"<<deviceList[i]<<"\t"<<deviceTag[i]<<endl;
    }
    ipOut.close();

    saveTags();

    cout<<"更改完成!"<<endl;
    system("pause");
    return;
}

void option2_diconnectDevice(){
    onLoad("断开指定设备连接",0);
    int disconnectNum;
    cout<<"----------------------------------------------------------------------"<<endl;
    cout<<"请输入需要断开连接设备编号(0返回,"<<deviceNum+1<<"断开全部):";
option2_diconnectDeviceInputNum:
    cin>>disconnectNum;
    if(disconnectNum==0){
        return;
    }else if(disconnectNum==deviceNum+1){
        fullCmd="adb kill-server";
        system(fullCmd.c_str());
        fullCmd="adb start-server";
        system(fullCmd.c_str());
        cout<<"断开所有连接(即重启)完成!"<<endl;
        deviceNum=0;
        system("pause");
        return;
    }else if(disconnectNum<0||disconnectNum>deviceNum+1){
        cout<<"错误!未知设备!请重新输入:";
        goto option2_diconnectDeviceInputNum;
    }else{
        preCmd="adb disconnect ";
        fullCmd=preCmd+deviceList[disconnectNum];
        cout<<"断开设备(名称:"<<deviceName[disconnectNum]<<"\t地址:"<<deviceList[disconnectNum]<<"\t标签:"<<deviceTag[disconnectNum]<<")完成!"<<endl;
        if(disconnectNum==deviceNum){
            deviceNum--;
        }else{
            for(int i=disconnectNum;i<=deviceNum;i++){
                deviceList[i]=deviceList[i+1];
                deviceName[i]=deviceName[i+1];
                deviceTag[i]=deviceTag[i+1];
            }
        }
        system("pause");
        return;
    }
    return;
}

void option2(){
    onLoad("已连接设备",1);
    setTags();
    // system("adb devices");
    fullCmd="adb devices";
    string result=getCmdResult(fullCmd);
    char res;
    int ippd=0,namepd=0;//ippd:判断是否查找ip    ipCurrent:ip的第几位   namepd:用来读取设备名称,skip=0时,后面不是设备名称  nameCurrent:设备名称的第几位
    deviceNum=0;
    for(int i=0;i<=result.length();i++){
        res=result[i];
        int ascii=res;
        if((ascii>=48&&ascii<=57)||ascii==46||ascii==58||namepd==1){//48~57:0~9  46is.   58is:  65~90:A~Z   97~122:a~z
            if(ippd==0){
                deviceNum++;
                deviceList[deviceNum]="";
                deviceName[deviceNum]="";
                ippd=1;
            }
            if((ascii>=48&&ascii<=57)||ascii==46||ascii==58) deviceList[deviceNum]+=res;
            if(namepd==1&&result[i+1]==9){
                namepd=0;
            }
            if(namepd==1&&ascii!=9&&ascii!=10){
                deviceName[deviceNum]+=res;
            }
            if(result[i+1]==9&&namepd!=1){
                namepd=1;
            }
        }else{
            ippd=0;
        }
    }
    if(deviceNum==0){
        cout<<"当前无设备连接"<<endl;
        system("pause");
        return;
    }

    //输出连接的设备
    fstream ipOut;
    remove("deviceList.json");
    ipOut.open("deviceList.json",ios::out);
    ipOut<<deviceNum<<endl;
    cout<<"当前共"<<deviceNum<<"个设备连接,信息如下:"<<endl;
    for(int i=1;i<=deviceNum;i++){
        cout<<i<<".设备名称:"<<deviceName[i]<<"\tip:"<<deviceList[i]<<"\t标签:"<<deviceTag[i]<<endl;
        ipOut<<deviceName[i]<<"\t"<<deviceList[i]<<endl;
    }
    ipOut.close();

    //操作
    cout<<"----------------------------------------------------------------------"<<endl;
    cout<<"0.返回主页"<<"\t\t\t"<<"1.更改设备标签"<<endl;
    cout<<"2.断开指定设备连接"<<endl;
    cout<<"请输入操作:";
option2InputAgain:
    cin>>option;
    switch (option){
        case '0':
            return;
        case '1':
            option2_chageTag();
            break;
        case '2':
            option2_diconnectDevice();
            break;
        default:
            cout<<"错误!未知操作!请重新输入:"<<endl;
            goto option2InputAgain;
            break;
    }
    return;
}

void option3(){
    onLoad("安装apk",0);
    preCmd="adb install ";
    for(;;){
        cout<<"----------------------------------------------------------------------"<<endl;
        cout<<"请输入安装位置(输入0退出):";
        cin>>path;
        if(path=="0"){
            return;
        }else{
            fullCmd=preCmd+path;
            cout<<fullCmd<<endl;
            system(fullCmd.c_str());
        }
    }
    
}

int main(){
    start();
    for(;;){
        onShow();
        cout<<"请输入操作并按下回车:";
getOption:
        cin>>option;
        switch(option){
            case '0':
                system("adb kill-server");
                system("pause");
                return 0;
            case '1':
                option1();
                break;
            case '2':
                option2();
                break;
            case '3':
                option3();
                break;
            default:
                cout<<"错误!未知操作!请重新输入:"<<endl;
                goto getOption;
        }
    }
    system("pause");
    return 0;
}